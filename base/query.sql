# QUERY URA

-- Procedure for consultation of CPF and CNPJ on the base PRI Telecom
DROP PROCEDURE IF EXISTS xcustomer.spr_check_cpf_cnpj_tri_telecom_bu;
CREATE PROCEDURE xcustomer.spr_check_cpf_cnpj_tri_telecom_bu(
    IN  p_cpf_cnpj bigint unsigned
)
BEGIN
    SELECT 
        btt.codigo,
        btt.tipo_pessoa,
        btt.subtipo_pessoa,
        btt.nome,
        btt.telefone,
        btt.email,
        btt.cidade,
        btt.bairro,
        btt.endereco,
        btt.data_nascimento,
        btt.cpf_cnpj,
        btt.sinc,
        btt.grupo
    FROM xcustomer.bu_tri_telecom btt
    WHERE btt.cpf_cnpj = p_cpf_cnpj
    ORDER BY btt.codigo
    LIMIT 1;    
END;

-- Table for URA reporting.
DROP TABLE IF EXISTS xcustomer.ura_history_report_tri_telecom;
CREATE TABLE xcustomer.ura_history_report_tri_telecom (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `nome_ura` varchar(50)  DEFAULT NULL,
  `id_fila` int(11) unsigned DEFAULT NULL,
  `nome_da_fila` varchar(50)  DEFAULT NULL,
  `ddr_da_ura` int(11) unsigned DEFAULT NULL,
  `tempo_da_opcao` int(11) unsigned DEFAULT NULL,
  `tempo_acumulado_ate_a_opcao` int(11) unsigned DEFAULT NULL,
  `opcao_ura` varchar(50) DEFAULT NULL,
  `opcao_ura_num` varchar(255) DEFAULT NULL,
  `texto_vocalizado` varchar(255) DEFAULT NULL,
  `entrada_dados` varchar(50) DEFAULT 'nao',
  `descricao_entrada` varchar(50) DEFAULT NULL,
  `valor_entrada` varchar(255) DEFAULT NULL,
  `retorno_dados` varchar(50) DEFAULT 'nao',
  `descricao_retorno` varchar(50) DEFAULT NULL,
  `valor_retorno` LONGTEXT DEFAULT NULL,
  `telefone` bigint(20) unsigned DEFAULT NULL,
  `uniqueid` varchar(50) DEFAULT NULL,
  `id_ticket` bigint(20) unsigned DEFAULT NULL,
  `status_da_opcao` varchar(50) DEFAULT NULL,
  `ligacao_desconectada` varchar(50) DEFAULT NULL,
  `tronco_utilizado` varchar(50) DEFAULT NULL,
  `transfer_status` varchar(50) DEFAULT NULL,
  `ura_cpf_cnpj` bigint(50) DEFAULT NULL,
  `agent_cpf_cnpj` bigint(50) DEFAULT NULL,
  `ura_opcoes_navegacao` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_id_ticket` (`id_ticket`),
  KEY `idx_uniqueid` (`uniqueid`),
  KEY `idx_data` (`data`),
  KEY `idx_hora` (`hora`),
  KEY `idx_telefone` (`telefone`)
) ENGINE=InnoDB;

-- Procedure to insert ura data in the table spr_check_cpf_cnpj_tri_telecom_bu
DROP PROCEDURE IF EXISTS xcustomer.spw_ura_history_tri_telecom;
CREATE PROCEDURE xcustomer.spw_ura_history_tri_telecom(
   IN p_nome_ura VARCHAR(50)
  ,IN p_id_fila VARCHAR(11)
  ,IN p_nome_da_fila VARCHAR(50)
  ,IN p_ddr_da_ura VARCHAR(11)
  ,IN p_tempo_da_opcao VARCHAR(11)
  ,IN p_tempo_acumulado_ate_a_opcao VARCHAR(11)
  ,IN p_opcao_ura VARCHAR(50)
  ,IN p_opcao_ura_num VARCHAR(255)  
  ,IN p_entrada_dados VARCHAR(50)
  ,IN p_descricao_entrada VARCHAR(50)
  ,IN p_valor_entrada VARCHAR(50)
  ,IN p_retorno_dados VARCHAR(50)
  ,IN p_descricao_retorno VARCHAR(50)
  ,IN p_valor_retorno LONGTEXT
  ,IN p_telefone VARCHAR(20)
  ,IN p_uniqueid VARCHAR(50)
  ,IN p_id_ticket VARCHAR(20)
  ,IN p_status_da_opcao VARCHAR(50)
  ,IN p_ligacao_desconectada VARCHAR(50)
  ,IN p_tronco_utilizado VARCHAR(50)
  ,IN p_texto_vocalizado VARCHAR(255)
  ,IN p_DIALSTATUS VARCHAR(255)
  ,IN p_ura_cpf_cnpj BIGINT(50)
  ,IN p_ura_opcoes_navegacao VARCHAR(255)
)
BEGIN

    select p_uniqueid;

    -- White variable adjustment
    IF p_nome_ura = "" OR p_nome_ura IS NULL OR p_nome_ura = " " THEN
        SET p_nome_ura = NULL;
    END IF;   

    IF p_id_fila = "" OR p_id_fila IS NULL OR p_id_fila = " " THEN
        SET p_id_fila = NULL;
    END IF;  

    IF p_nome_da_fila = "" OR p_nome_da_fila IS NULL OR p_nome_da_fila = " " THEN
        SET p_nome_da_fila = NULL;
    END IF;    

    IF p_ddr_da_ura = "" OR p_ddr_da_ura IS NULL OR p_ddr_da_ura = " " THEN
        SET p_ddr_da_ura = NULL;
    END IF;   

    IF p_tempo_da_opcao = "" OR p_tempo_da_opcao IS NULL OR p_tempo_da_opcao = " " THEN
        SET p_tempo_da_opcao = NULL;
    END IF;   

    IF p_tempo_acumulado_ate_a_opcao = "" OR p_tempo_acumulado_ate_a_opcao IS NULL OR p_tempo_acumulado_ate_a_opcao = " " THEN
        SET p_tempo_acumulado_ate_a_opcao = NULL;
    END IF;     

    IF p_opcao_ura = "" OR p_opcao_ura IS NULL OR p_opcao_ura = " " THEN
        SET p_opcao_ura = NULL;
    END IF;   

    IF p_opcao_ura_num = "" OR p_opcao_ura_num IS NULL OR p_opcao_ura_num = " " THEN
        SET p_opcao_ura_num = NULL;
    END IF;  

    IF p_entrada_dados = "" OR p_entrada_dados IS NULL OR p_entrada_dados = " " THEN
        SET p_entrada_dados = NULL;
    END IF; 

    IF p_descricao_entrada = "" OR p_descricao_entrada IS NULL OR p_descricao_entrada = " " THEN
        SET p_descricao_entrada = NULL;
    END IF;  

    IF p_valor_entrada = "" OR p_valor_entrada IS NULL OR p_valor_entrada = " " THEN 
        SET p_valor_entrada = NULL;
    END IF; 

    IF p_retorno_dados = "" OR p_retorno_dados IS NULL OR p_retorno_dados = " " THEN
        SET p_retorno_dados = NULL;
    END IF;   

    IF p_descricao_retorno = "" OR p_descricao_retorno IS NULL OR p_descricao_retorno = " " THEN
        SET p_descricao_retorno = NULL;
    END IF;    

    IF p_valor_retorno = "" OR p_valor_retorno IS NULL OR p_valor_retorno = " " THEN
        SET p_valor_retorno = NULL;
    END IF;   

    IF p_telefone = "" OR p_telefone IS NULL OR p_telefone = " " THEN
        SET p_telefone = NULL;
    END IF;      

    IF p_uniqueid = "" OR p_uniqueid IS NULL OR p_uniqueid = " " THEN
        SET p_uniqueid = NULL;
    END IF;      

    IF p_status_da_opcao = "" OR p_status_da_opcao IS NULL OR p_status_da_opcao = " " THEN
        SET p_status_da_opcao = NULL;
    END IF;  

    IF p_ligacao_desconectada = "" OR p_ligacao_desconectada IS NULL OR p_ligacao_desconectada = " " THEN
        SET p_ligacao_desconectada = NULL;
    END IF;       

    IF p_tronco_utilizado = "" OR p_tronco_utilizado IS NULL OR p_tronco_utilizado = " " THEN
        SET p_tronco_utilizado = NULL;
    END IF;      

    IF p_texto_vocalizado = "" OR p_texto_vocalizado IS NULL OR p_texto_vocalizado = " " THEN
        SET p_texto_vocalizado = NULL;
    END IF;

    IF p_DIALSTATUS = "" OR p_DIALSTATUS IS NULL OR p_DIALSTATUS = " " THEN
        SET p_DIALSTATUS = NULL;
    END IF;       

    IF p_ura_cpf_cnpj = "" OR p_ura_cpf_cnpj IS NULL OR p_ura_cpf_cnpj = " " THEN
        SET p_ura_cpf_cnpj = NULL;
    END IF;       

    IF p_ura_opcoes_navegacao = "" OR p_ura_opcoes_navegacao IS NULL OR p_ura_opcoes_navegacao = " " THEN
        SET p_ura_opcoes_navegacao = NULL;
    END IF;       

    -- Insert to base                          
    INSERT INTO xcustomer.ura_history_report_tri_telecom                                                                                                                                                                                                                                                                               
           (data, hora, nome_ura, id_fila, nome_da_fila, ddr_da_ura, tempo_da_opcao, tempo_acumulado_ate_a_opcao, opcao_ura, opcao_ura_num, entrada_dados, descricao_entrada, valor_entrada, retorno_dados, descricao_retorno, valor_retorno, telefone, uniqueid, id_ticket, status_da_opcao, ligacao_desconectada, tronco_utilizado, texto_vocalizado, transfer_status, ura_cpf_cnpj, ura_opcoes_navegacao)     
    VALUES                                                                                                                                                                                                                                                                                                         
        (CURRENT_DATE(), CURRENT_TIME(), p_nome_ura, p_id_fila, p_nome_da_fila, p_ddr_da_ura, p_tempo_da_opcao, p_tempo_acumulado_ate_a_opcao, p_opcao_ura, p_opcao_ura_num, p_entrada_dados, p_descricao_entrada, p_valor_entrada, p_retorno_dados, p_descricao_retorno, p_valor_retorno, p_telefone, p_uniqueid, p_id_ticket, p_status_da_opcao, p_ligacao_desconectada, p_tronco_utilizado, p_texto_vocalizado, p_DIALSTATUS, p_ura_cpf_cnpj, p_ura_opcoes_navegacao);

END;

# QUERY XCALL AGENT

# create module 83
INSERT INTO xcall.xcall_modules_cad (id, activated, module, descr) VALUES (94,1,"xModule83", "TRI Telecom");

# Create schema
CREATE DATABASE xmodule83   
    CHARACTER SET latin1
    COLLATE latin1_general_ci;	

# Procedure that inserts cpf_cnpj collected by the agente into the spr_check_cpf_cnpj_tri_telecom_bu  
 	DROP PROCEDURE IF EXISTS xmodule83.spw_ura_history_tri_telecom_agent;
    CREATE PROCEDURE xmodule83.spw_ura_history_tri_telecom_agent(
         IN p_id_ticket VARCHAR(20)
        ,IN p_agent_cpf_cnpj BIGINT(50)
    )
    BEGIN

        select p_id_ticket;

        -- adjust null variables
        IF p_agent_cpf_cnpj = "" OR p_agent_cpf_cnpj IS NULL OR p_agent_cpf_cnpj = " " THEN
            SET p_agent_cpf_cnpj = NULL;
        END IF;       
    
        -- update data in the base                        
        UPDATE xcustomer.ura_history_report_tri_telecom u
            SET u.agent_cpf_cnpj = p_agent_cpf_cnpj
            WHERE u.id_ticket = p_id_ticket
            ORDER BY u.id DESC LIMIT 1;
                    
    END;